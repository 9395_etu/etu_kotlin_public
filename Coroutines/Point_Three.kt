import kotlinx.coroutines.*
import kotlin.random.Random

suspend fun First():Int
{
    delay(1000L)
    return Random.nextInt(0,15)

}
suspend fun Second():Int
{
    delay(1500L)
    return Random.nextInt(0,15)
}

suspend fun main() = coroutineScope{
    println("I'm sleeping 0 ...")
    delay(1000L)
    println(" I'm sleeping 1 ...")
    delay(1000L)
    println("  I'm sleeping 2 ...\n")
    delay(1000L)
    launch { println("main: I'm tired of waiting! I'm running finally") }
    val Firstmessage: Deferred<Int> = async {First()}
    val Secondmessage: Deferred<Int> = async{Second()}
    val result = Firstmessage.await() + Secondmessage.await()
    println("main: Now I can quit.»")
}
